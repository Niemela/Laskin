﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laskin
{
    class Program
    {
        static void Main(string[] args)
        {


            Dictionary<char, Laskin> laskin = new Dictionary<char, Laskin>();

            Laskin summa = new Yhteenlaskin();
            Laskin erotus = new Vahennyslaskin();
            Laskin kerto = new Kertolaskin();
            Laskin jako = new Jakolaskin();

            laskin.Add('-', erotus);
            laskin.Add('+', summa);
            laskin.Add('*', kerto);
            laskin.Add('/', jako);




            Console.WriteLine("Anna luku1");
            int Luku1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Anna operaatio");
            char operaatio = char.Parse(Console.ReadLine());

            Console.WriteLine("Anna luku2");
            int Luku2 = int.Parse(Console.ReadLine());
 


            Laskin tulos = laskin[operaatio];
            tulos.setLuku1(Luku1);
            tulos.setLuku2(Luku2);
            Console.WriteLine(tulos.Laske());



        }
    }
}
