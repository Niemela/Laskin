﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laskin
{
    public abstract class Laskin
    {
        protected int luku1;
        protected int luku2;


        public int getLuku1()
        {
            return luku1;
        }

        public void setLuku1(int Luku1)
        {
            luku1 = Luku1;
        }
        public int getLuku2()
        {
            return luku2;
        }

        public void setLuku2(int Luku2)
        {
            luku2 = Luku2;
        }

        public abstract double Laske();
        
    }

    public class Yhteenlaskin : Laskin
    {
        public override double Laske()
        {
            return (luku1 + luku2);
        }
    }

    public class Vahennyslaskin : Laskin
    {
        public override double Laske()
        {

            return (getLuku1() - getLuku2());

        }

    }

    public class Jakolaskin : Laskin
    {
        public override double Laske()
        {
            return (luku1 / luku2);
        }

    }

    public class Kertolaskin : Laskin
    {
        public override double Laske()
        {
            return (luku1 * luku2);
        }

    }

}
